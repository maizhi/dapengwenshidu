local tmr = tmr

module('button')
 
function debounce (func)
    local last = 0
    local delay = 200000 --200ms
 
    return function (...)
        local now = tmr.now()
        local delta = now - last
        if delta < 0 then delta = delta + 2147483647 end;
        if delta < delay then return end;
 
        last = now
        return func(...)
    end
end 

