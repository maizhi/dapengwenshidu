package com.myfojing.mycloud;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/* 传感器数字记录 */

public class MyRecord extends RealmObject {
    private String mac;
    private float temp;
    private float hum;
    private long created_at;

    public MyRecord() {
    }

    MyRecord(String mac, float temp, float hum, long created_at) {
        this.mac = mac;
        this.temp = temp;
        this.hum = hum;
        this.created_at = created_at;
    }

    float getTemp() {
        return temp;
    }

    float getHum() {
        return hum;
    }

    long getCreated_at() {
        return created_at * 1000;  // 返回毫秒
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    void setTemp(float temp) {
        this.temp = temp;
    }

    void setHum(float hum) {
        this.hum = hum;
    }

    void setCreated_at(long created_at) {
        this.created_at = created_at;
    }
}
