package com.myfojing.mycloud;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;


class MainListviewAdapter extends BaseAdapter {

    private ArrayList<Sensirion> list;
    private Activity activity;
    private View.OnClickListener listener;

    public MainListviewAdapter(Activity activity, ArrayList<Sensirion> list,
                               View.OnClickListener listener) {
        super();
        this.activity = activity;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.main_listview_adapter, null);
            holder = new ViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.main_title);
            holder.temp = (TextView) convertView.findViewById(R.id.main_temp);
            holder.hum = (TextView) convertView.findViewById(R.id.main_hum);
            //holder.last_update = (TextView) convertView.findViewById(R.id.main_last_update);
            //holder.Status = (TextView) convertView.findViewById(R.id.main_status);
            holder.curd = (ImageButton) convertView.findViewById(R.id.sensirion_curd);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Sensirion model = list.get(position);
        holder.title.setText(model.getTitle());
        holder.temp.setText(model.getTemp());
        holder.hum.setText(model.getHum());
        //holder.last_update.setText(model.getLast_update());
        //holder.Status.setText(model.getStatus());
        holder.curd.setTag(position);
        holder.curd.setOnClickListener(this.listener);

        holder.curd.setTag(position);
        holder.curd.setOnClickListener(this.listener);

        if (convertView.isSelected())
            convertView.setBackgroundColor(Color.RED);

        return convertView;
    }

    private class ViewHolder {
        TextView title;
        TextView temp;
        TextView hum;
        //TextView last_update;
        //TextView Status;
        ImageButton curd;
    }
}
