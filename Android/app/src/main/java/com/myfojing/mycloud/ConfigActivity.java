package com.myfojing.mycloud;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class ConfigActivity extends AppCompatActivity
        implements
        ConfigWifiFragment.OnFragmentInteractionListener,
        ConfigNetworkFragment.OnFragmentInteractionListener,
        ConfigDeviceFragment.OnFragmentInteractionListener,
        ConfigDdnsFragment.OnFragmentInteractionListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private ConfigWifiFragment configWifiFragment;
    private ConfigNetworkFragment configNetworkFragment;
    private ConfigDeviceFragment configDeviceFragment;
    private ConfigDdnsFragment configDdnsFragment;

    private String ip_config;
    private String port_config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //接收参数：IP，PORT
        Sensirion sensirion = getIntent().getParcelableExtra("sensirion");
        Intent intent = getIntent();
        ip_config = intent.getStringExtra("ip_config");
        port_config = intent.getStringExtra("port_config");

        configNetworkFragment = ConfigNetworkFragment.newInstance(
                sensirion.getIp(),
                sensirion.getNm(),
                sensirion.getGw(),
                sensirion.getPort()
        );
        configDeviceFragment = ConfigDeviceFragment.newInstance(
                sensirion.getTitle(),
                sensirion.getDescription(),
                sensirion.getType(),
                sensirion.getPIN(),
                ip_config,
                port_config
        );

        configDdnsFragment = ConfigDdnsFragment.newInstance(ip_config, port_config);
        configWifiFragment = ConfigWifiFragment.newInstance(ip_config, port_config);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(URI uri) {

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 4;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return configWifiFragment;
                case 1:
                    return configNetworkFragment;
                case 2:
                    return configDeviceFragment;
                default:
                    return configDdnsFragment;
            } // switch
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;  // 3
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.conf_wifi);
                case 1:
                    return getString(R.string.conf_network);
                case 2:
                    return getString(R.string.conf_device);
                case 3:
                    return getString(R.string.DDNS);
            }
            return null;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
// end
}
