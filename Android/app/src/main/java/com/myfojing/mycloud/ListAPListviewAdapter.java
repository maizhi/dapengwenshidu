package com.myfojing.mycloud;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

class ListAPListviewAdapter extends BaseAdapter {

    private ArrayList<HashMap<String, String>> list;
    private Activity activity;

    public ListAPListviewAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_ap_listview_adapter, null);
            holder = new ViewHolder();
            holder.index = (TextView) convertView.findViewById(R.id.list_ap_index);
            holder.ssid = (TextView) convertView.findViewById(R.id.list_ap_ssid);
            //holder.encyrpt = (TextView) convertView.findViewById(R.id.list_ap_encrypt);
            holder.quality = (ImageView) convertView.findViewById(R.id.list_ap_quality);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> item = list.get(position);
        holder.index.setText(item.get("index"));
        holder.ssid.setText(item.get("ssid"));
        //holder.encyrpt.setText(item.get("encrypt"));

        int rssi = Integer.parseInt(item.get("rssi"));
        if (rssi >= -55) {
            holder.quality.setImageDrawable(ContextCompat.getDrawable(activity.getApplication(),
                    R.drawable.percent_100));
        } else if (rssi >= -70) {
            holder.quality.setImageDrawable(ContextCompat.getDrawable(activity.getApplication(),
                    R.drawable.percent_060));
        } else if (rssi >= -85) {
            holder.quality.setImageDrawable(ContextCompat.getDrawable(activity.getApplication(),
                    R.drawable.percent_020));
        } else {
            holder.quality.setImageDrawable(ContextCompat.getDrawable(activity.getApplication(),
                    R.drawable.percent_005));
        }

        holder.quality.setImageLevel(rssi);

        return convertView;
    }

    private class ViewHolder {
        TextView index;
        TextView ssid;
        //TextView encyrpt;
        ImageView quality;
    }
    // end
}
