package com.myfojing.mycloud;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

class WifiCacheListviewAdapter extends BaseAdapter {

    private ArrayList<HashMap<String, String>> list;
    private Activity activity;
    private int index_ap;

    public WifiCacheListviewAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    public void setApIndex(int index_ap) {
        this.index_ap = index_ap;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.wifi_cache_listview_adapter, null);
            holder = new ViewHolder();
            holder.index = (TextView) convertView.findViewById(R.id.wifi_cache_index);
            holder.ssid = (TextView) convertView.findViewById(R.id.wifi_cache_ssid);
            holder.pwd = (TextView) convertView.findViewById(R.id.wifi_cache_pwd);
            holder.selected = (ImageView) convertView.findViewById(R.id.wifi_cache_selected);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> item = list.get(position);
        holder.index.setText(item.get("index"));
        holder.ssid.setText(item.get("ssid"));
        holder.pwd.setText(item.get("pwd"));

        if (index_ap == Integer.parseInt(item.get("index"))) {
            holder.selected.setImageDrawable(ContextCompat.getDrawable(
                    activity.getApplication(), R.drawable.wifi_selected));
        } else {
            holder.selected.setVisibility(View.INVISIBLE);
        }


        return convertView;
    }

    private class ViewHolder {
        TextView index;
        TextView ssid;
        TextView pwd;
        ImageView selected;
    }
    // end
}
