package com.myfojing.mycloud;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;


public class ConfigDeviceFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String arg_title = "title";
    private static final String arg_desc = "desc";
    private static final String arg_type = "type";
    private static final String arg_pin = "pin";

    private String title;
    private String desc;
    private String type;
    private String pin;
    private String ip;
    private String port;

    private Sensirion sensirion;
    private Handler handler;

    private OnFragmentInteractionListener mListener;
    private EditText txt_title;
    private EditText txt_desc;
    private Spinner txt_type;
    private EditText txt_pin;

    private Button btnSave;
    private Button btnRefresh;
    private Button btnSetTime;

    public ConfigDeviceFragment() {
        // Required empty public constructor
    }


    public static ConfigDeviceFragment newInstance(String title, String desc, String type, String pin,
                                                   String ip, String port) {
        ConfigDeviceFragment fragment = new ConfigDeviceFragment();
        Bundle args = new Bundle();
        args.putString(arg_title, title);
        args.putString(arg_desc, desc);
        args.putString(arg_type, type);
        args.putString(arg_pin, pin);

        args.putString("ip", ip);
        args.putString("port", port);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(arg_title);
            desc = getArguments().getString(arg_desc);
            type = getArguments().getString(arg_type);
            pin = getArguments().getString(arg_pin);

            ip = getArguments().getString("ip");
            port = getArguments().getString("port");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_config_device, container, false);
        // ui
        txt_title = (EditText) view.findViewById(R.id.txt_title);
        txt_desc = (EditText) view.findViewById(R.id.txt_desc);
        txt_type = (Spinner) view.findViewById(R.id.txt_type);
        txt_pin = (EditText) view.findViewById(R.id.txt_pin);
        // 按钮
        btnSave = (Button) view.findViewById(R.id.btnSaveDevice);
        btnRefresh = (Button) view.findViewById(R.id.btnRefreshDevice);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("title", txt_title.getText());
                    object.put("desc", txt_desc.getText());
                    object.put("type", txt_type.getSelectedItem());
                    object.put("pin", txt_pin.getText());
                    sensirion.write_device(object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } // onclick
        });

        btnSetTime = (Button) view.findViewById(R.id.btnSetTime);
        btnSetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long longtime = System.currentTimeMillis() / 1000;
                sensirion.set_time(longtime.toString());
            }
        });

        handler = new Handler();
        sensirion = new Sensirion(ip, port);

        sensirion.read_device();
        sensirion.setSensirionListener(new Sensirion.SensirionListener() {
            @Override
            public void onDataReady(JSONObject data) {
                try {
                    // 读取设备信息
                    if (data.getString("cmd").equals("read_device")) {
                        title = data.getString("title");
                        desc = data.getString("desc");
                        type = data.getString("type");
                        pin = data.getString("PIN");
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                txt_title.setText(title);
                                txt_desc.setText(desc);

                                String[] array = getResources().getStringArray(R.array.sensirion_type);
                                int pos = 0;
                                for (String s : array) {
                                    if (s.equals(type)) {
                                        txt_type.setSelection(pos);
                                        break;
                                    }
                                    pos++;
                                }
                                txt_pin.setText(pin);
                            }
                        });
                    }

                    if (data.getString("cmd").equals("write_device")) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(), "网络写入", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(URI uri);
    }
}
