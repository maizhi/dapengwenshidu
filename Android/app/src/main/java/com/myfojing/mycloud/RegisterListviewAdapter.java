package com.myfojing.mycloud;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


class RegisterListviewAdapter extends BaseAdapter {
    private ArrayList<Sensirion> list;
    private Activity activity;

    public RegisterListviewAdapter(Activity activity, ArrayList<Sensirion> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    private class ViewHolder {
        TextView title;
        TextView ip;
        TextView port;
        TextView type;
        TextView mac;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater =  activity.getLayoutInflater();

        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.discovery_listvew_adapter, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.ds_name);
            holder.ip = (TextView) convertView.findViewById(R.id.ds_ip);
            holder.port = (TextView) convertView.findViewById(R.id.ds_port);
            holder.type = (TextView) convertView.findViewById(R.id.ds_type);
            holder.mac = (TextView) convertView.findViewById(R.id.ds_mac);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        Sensirion model = list.get(position);
        holder.title.setText(model.getTitle());
        holder.ip.setText(model.getIp());
        holder.port.setText(model.getPort());
        holder.type.setText(model.getType());
        holder.mac.setText(model.getMac());
/*
        if (convertView.isSelected())
            convertView.setBackgroundColor(Color.RED);*/

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
