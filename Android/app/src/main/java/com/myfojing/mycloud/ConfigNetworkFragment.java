package com.myfojing.mycloud;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;


public class ConfigNetworkFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String arg_ip = "ip";
    private static final String arg_nm = "nm";
    private static final String arg_gw = "gw";
    private static final String arg_port = "port";

    private String ip;
    private String nm;
    private String gw;
    private String port;

    private EditText txt_ip;
    private EditText txt_nm;
    private EditText txt_gw;
    private EditText txt_port;

    private EditText txt_dns_1;
    private EditText txt_dns_2;

    private Button btnSendNetwork;
    private Handler handler;

    private Sensirion sensirion;

    private OnFragmentInteractionListener mListener;

    public ConfigNetworkFragment() {

    }


    public static ConfigNetworkFragment newInstance(String ip, String nm, String gw, String port) {
        ConfigNetworkFragment fragment = new ConfigNetworkFragment();
        Bundle args = new Bundle();
        args.putString(arg_ip, ip);
        args.putString(arg_nm, nm);
        args.putString(arg_gw, gw);
        args.putString(arg_port, port);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (getArguments() != null) {
            ip = getArguments().getString(arg_ip);
            nm = getArguments().getString(arg_nm);
            gw = getArguments().getString(arg_gw);
            port = getArguments().getString(arg_port);

            sensirion = new Sensirion(ip, port);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_config_network, container, false);

        txt_ip = (EditText) view.findViewById(R.id.txt_ip);
        txt_nm = (EditText) view.findViewById(R.id.txt_nm);
        txt_gw = (EditText) view.findViewById(R.id.txt_gw);
        txt_port = (EditText) view.findViewById(R.id.txt_port);

        txt_dns_1 = (EditText) view.findViewById(R.id.txt_dns_1);
        txt_dns_2 = (EditText) view.findViewById(R.id.txt_dns_2);

        btnSendNetwork = (Button) view.findViewById(R.id.btnSendNetwork);
        btnSendNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("ip", txt_ip.getText());
                    object.put("netmask", txt_nm.getText());
                    object.put("gateway", txt_gw.getText());
                    object.put("port", txt_port.getText());
                    object.put("port", txt_port.getText());
                    object.put("dns_1", txt_dns_1.getText());
                    object.put("dns_2", txt_dns_2.getText());
                    sensirion.write_network(object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        sensirion.setSensirionListener(new Sensirion.SensirionListener() {
            @Override
            public void onDataReady(JSONObject data) {
                try {
                    // 读取network
                    if (data.getString("cmd").equals("write_network")) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(), "写入", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        txt_ip.setText(ip);
        txt_nm.setText(nm);
        txt_gw.setText(gw);
        txt_port.setText(port);

        return view;
    }

     @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(URI uri);
    }
}
