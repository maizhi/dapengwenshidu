package com.myfojing.mycloud;


import io.realm.Realm;
import io.realm.RealmResults;

class SensirionHelper {

    public SensirionHelper() {
    }

    // 新建 / 更新
    static void save_or_update(Sensirion sensirion, Realm realm) {

        final String title = sensirion.getTitle();
        final String description = sensirion.getDescription();
        final String type = sensirion.getType();
        final String pin = sensirion.getPIN();
        final String ip = sensirion.getIp();
        final String nm = sensirion.getNm();
        final String gw = sensirion.getGw();
        final String port = sensirion.getPort();
        final String mac = sensirion.getMac();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Sensirion mySensirion = realm.createObject(Sensirion.class, mac);
                mySensirion.setTitle(title);
                mySensirion.setDescription(description);
                mySensirion.setType(type);
                mySensirion.setPIN(pin);
                mySensirion.setIp(ip);
                mySensirion.setNm(nm);
                mySensirion.setGw(gw);
                mySensirion.setPort(port);
                realm.copyToRealmOrUpdate(mySensirion);
            }
        });
    }

    // 删除
    static void delete_sensirion(final String mac, Realm realm) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Sensirion> sensirions = realm.where(Sensirion.class).equalTo("mac", mac).findAll();
                sensirions.deleteAllFromRealm();
            }
        });
    }

    // 取所有结果
    static RealmResults<Sensirion> get_all(Realm realm) {
        return realm.where(Sensirion.class).findAll();
    }
}
