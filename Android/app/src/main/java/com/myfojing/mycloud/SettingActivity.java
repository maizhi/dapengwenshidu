package com.myfojing.mycloud;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class SettingActivity extends AppCompatActivity {

    final private String TAG = "我的Setting调试";
    private CheckBox enable_service;
    private boolean interval_is_changed = false;
    private EditText sample_interval;
    private EditText record_interval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Log.d(TAG, "设置onCreate()");
        // ui
        enable_service = (CheckBox) findViewById(R.id.enable_sample_service);
        sample_interval = (EditText) findViewById(R.id.setting_sample_interval);
        record_interval = (EditText) findViewById(R.id.setting_record_interval);
        // read data
        read();
        sample_interval.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                interval_is_changed = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                // 容错
                if (s.toString().equals("0") || s.toString().isEmpty()) {
                    sample_interval.setText("1");
                }

            }
        });

    }

    public void onSettingApplyClick(View view) {
        SharedPreferences.Editor editor = getSharedPreferences("profile", MODE_PRIVATE).edit();
        editor.putBoolean("sample_service_enabled", enable_service.isChecked());
        if (interval_is_changed) {
            String interval = sample_interval.getText().toString();
            editor.putString("sample_interval", interval);
            interval_is_changed = false;
        }
        editor.putString("sample_interval", record_interval.getText().toString());
        editor.apply();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("in1", Integer.parseInt(sample_interval.getText().toString()));
        intent.putExtra("in2", Integer.parseInt(record_interval.getText().toString()));
        setResult(RESULT_OK, intent);
        finish();
    }

    public void onSettingCancelClick(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }


    private void read() {
        SharedPreferences profile = getSharedPreferences("profile", MODE_PRIVATE);
        enable_service.setChecked(profile.getBoolean("sample_service_enabled", true));
        sample_interval.setText(profile.getString("sample_interval", "1"));
        record_interval.setText(profile.getString("record_interval", "1"));
    }
    // end
}
